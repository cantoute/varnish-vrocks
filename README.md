# Varnish Rocks

## Multipurpose configuration for varnish

### tested with version

- 6.1 (on Debian 10)

I have also made a script that sets up a high performance web server based on Debian.

https://github.com/cantoute/ng-lamp

## Requirements

This configuration was designed for varnish 7.0

  ```bash
  curl -s https://packagecloud.io/install/repositories/varnishcache/varnish70/script.deb.sh | sudo bash
  apt install varnish

  systemctl edit --full varnish
  ```

  - Limit access to loopback interface
  - Add 1 line for listening to port 6091

  ```
  ExecStart=/usr/sbin/varnishd \
          -a localhost:6081 \
          -a localhost:6091 \
          -a localhost:8443,PROXY \
          -p feature=+http2 \
          -f /etc/varnish/default.vcl \
          -s malloc,256m
  ExecReload=/usr/sbin/varnishreload
  ```

  ```bash
  systemctl restart varnish
  ```

  [Install varnish on debian](https://www.varnish-software.com/developers/tutorials/installing-varnish-debian/)

- varnish-modules.

https://varnish-cache.org/vmods/

  ```bash
  apt install varnish-dev python-sphinx make automake libtool
  cd /root
  git clone https://github.com/varnish/varnish-modules.git
  cd varnish-modules
  git checkout 7.0

  ./bootstrap
  ./configure   # run "configure -h" first to list options
  make
  #make check    # optional (tests)
  # Note: a building from source you need to run make rst-docs before being able to install.
  make rst-docs # optional (docs)
  make install  # optional (installation), run as root
  ```

- Vrocks

  ```bash
  cd /etc/varnish
  git clone https://gitlab.com/cantoute/varnish-vrocks.git
  ln -s varnish-vrocks/vrocks ./
  mv default.vcl default.vcl.dist
  cp varnish-vrocks/vrocks-example.default.vcl default.vcl
  varnish-vrocks/varnishconfigtest
  ```

### Uses

- saintmode
- headers

---

# Install

_not all up to date_

The concept is that most of varnish behavior is controlled by headers passed by reverse proxy.

| Header                                                                                           | Type     |   Default |
| ------------------------------------------------------------------------------------------------ | -------- | --------: |
| X-VR-Enabled                                                                                     | boolean  |      true |
| X-VR-Debug                                                                                       | boolean  |     false |
| X-VR-Allow-Purge-Port                                                                            | number   | _not set_ |
| X-VR-Retry-If-Error                                                                              | boolean  |     false |
| X-VR-Stale-If-Error _(hard coded to 10d)_                                                        | boolean  |      true |
| X-VR-Stale-If-Bot <br>_(max 10d - if set to true default 1d)_ <br>_Requires X-VR-Stale-If-Error_ | duration |     false |
| X-VR-Rule-Wordpress                                                                              | boolean  |     false |
| X-VR-Rule-Prestashop                                                                             | boolean  |     false |
| X-VR-Clear-Cookie-All                                                                            | boolean  |     false |
| X-VR-Clear-Cookie-PHPSESSID                                                                      | boolean  |     false |
| X-VR-Cache-Ajax                                                                                  | boolean  |     false |
| X-VR-Cache-Static                                                                                | boolean  |     false |
| X-VR-Force-Reload-Allow                                                                          | boolean  |      true |
| X-VR-Default-TTL _(Varnish builtin 2m)_                                                          | duration | _not set_ |
| X-VR-Default-Grace _(builtin 10s)_                                                               | duration | _not set_ |
| X-VR-Default-Keep<br> _(Varnish 6.x: this only affects 304 response)_                            | duration |        3m |
| X-VR-Clean-Cache-Control<br> _removes directives specific to reverse proxy cache_                | boolean  |     false |
| X-VR-Query-String-Sort<br> _enable sort QueryString passed to backend_                           | boolean  |     false |

---

## Using nginx as reverse proxy

An example of nginx configuration is in [vrocks/example.nginx.conf](vrocks/example.nginx.conf)
