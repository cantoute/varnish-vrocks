#
# This is an example VCL file for Varnish.
#
# It does not do anything by default, delegating control to the
# builtin VCL. The builtin VCL is called when there is no explicit
# return statement.
#
# See the VCL chapters in the Users Guide at https://www.varnish-cache.org/docs/
# and https://www.varnish-cache.org/trac/wiki/VCLExamples for more examples.

# Marker to tell the VCL compiler that this VCL has been adapted to the
# new 4.0 format.
vcl 4.0;

import directors;

# Requires varnish-modules
import saintmode;
import header;
import var;

# Default backend definition. Set this to point to your content server.
backend default {
  .host = "127.0.0.1";
  .port = "8081";

  .max_connections = 100;
  .connect_timeout = 1.0s;
  .first_byte_timeout = 30s;
  .between_bytes_timeout = 60s;

#   .probe = {
#     .url = "/ping";
## or
#     .request =
#       "GET /ping HTTP/1.1"
#       "Host: localhost"
#       "User-Agent: varnish-health"
#       "Connection: close";
#
#     .timeout = 1s;
#     .interval = 1s;
#     .window = 3;
#     .threshold = 2;
#   }
}

# backend fallback {
#   .host = "127.0.0.1";
#   .port = "8080";
# }

acl upstream_proxy {
  "localhost";
  "127.0.0.1";
  "::1";
  # cloud external interface
  "127.0.1.1";
}

# Who can purge the cache
# in most cases, using server external ip is safe
# allowing localhost behind nginx would allow any ip !!
# so purge only on http://localhost:9091 (not accessible from remote)
acl purge_acl {
  # "123.123.123.123";
}

sub vcl_recv {
  # dummy to avoid error acl not used
  if ( false
    && (
      client.ip ~ upstream_proxy
      || client.ip ~ purge_acl
    ) ) {}
}

sub vcl_recv {
  # set req.http.X-VR-Debug = "false";
  
  # Alternative to using purge_acl is to set up varnish
  # to listen to a separate port that can only be accessed from localhost
  # sudo systemctl edit --full varnish


/******************************************************
# /etc/systemd/system/varnish.service.d/customexec.conf

[Service]
ExecStart=
ExecStart=/usr/sbin/varnishd -j unix,user=vcache -F \
                        -a localhost:6081 \
                        -a localhost:6091 \
                        -p thread_pool_min=45 \
                        -p thread_pool_max=450 \
                        -p thread_pool_timeout=300 \
                        -f /etc/varnish/default.vcl \
                        -S /etc/varnish/secret \
                        -s malloc,256M



# on debian next line will create a systemd template in /lib/systemd/system/varnish.service
# sudo systemctl edit --full varnish
# sudo systemctl daemon-reload && sudo service varnish restart
*****************************************************/

}


sub vcl_recv {
  # Checkout ttps://www.varnish-software.com/developers/tutorials/installing-varnish-debian/
  # on debian next line will create a systemd template in /lib/systemd/system/varnish.service
  # sudo systemctl edit --full varnish
  # then add a line: -a localhost:6091
  # you don't want to expose this port to the public
  # apply:
  # sudo systemctl daemon-reload && sudo service varnish restart
  # then set localhost:6091 as your varnish server in w3-total-cache
}

############################################################################
# saintmode
##
# using saintmode is not required at all but comes handy as to use multiple backends

sub vcl_init {
  # saintmode.blacklist() called in lib/backend-error.vcl
  # var.set_duration("blacklist_5xx", 10s); # status 5xx
  # var.set_duration("blacklist_5xx_bgfetch", 30s); # status 5xx on background fetch
  # var.set_duration("blacklist_be_failed", 30s); # vcl_backend_error

  # 10 5xx errors in 10s will trigger backend sick
  new sm1 = saintmode.saintmode(default, 10); 
  #new sm2 = saintmode.saintmode(default, 10);

  new director = directors.round_robin();
  director.add_backend(sm1.backend());
  # director.add_backend(sm2.backend());
}

# sub vcl_backend_fetch {
#   # Get a backend from the director.
#   # When returning a backend, the director will only return backends
#   # saintmode says are healthy.
#   set bereq.backend = sm.backend();
# }

# sub vcl_recv {
#   set req.backend_hint = director.backend();
# }

sub vcl_backend_fetch {
  # Get a backend from the director.
  # When returning a backend, the director will only return backends
  # saintmode says are healthy.
  set bereq.backend = director.backend();
}

# sub vcl_backend_fetch {
#   set bereq.backend = sm1.backend();
#   set bereq.http.X-VR-Debug-Msg = bereq.http.X-VR-Debug-Msg + ", backend saintmode";
# }


sub vcl_recv {
  if (req.url ~ "/saintmode-status") {
    return (synth(700, "OK"));
  }
}

sub vcl_synth {
  if (resp.status == 700) {
    synthetic(saintmode.status());
    return (deliver);
  }
}


sub vcl_deliver {
  # Happens when we have all the pieces we need, and are about to send the
  # response to the client.
  #
  # You can do accounting or modifying the final object here.
  # if ( ! std.healthy(req.backend_hint) )
  # {
  #
  # }
}

# /saintmode
############################################################################


sub vcl_recv {
  # Happens before we check if we have this in cache already.
  #
  # Typically you clean up the request here, removing cookies you don't need,
  # rewriting the request, etc.
  if (req.http.Host == "www.example.com" )
  {
    # set req.http.X-VR-Debug = "true";
  }
}

# redirect mobiles to amp version
# sub vcl_recv {
#   if (
#     req.http.Host == "example-with-amp.com"
#     # request without dots
#     && ! req.url ~ "\."
#     && ! req.url ~ "/feed"
#     && ! req.url ~ "/wp-"
#     ) {
#     # using wordpress with pretty permalinks, good chances this is a page

#     # fix /example/amp/amp
#     if ( req.url ~ "(/amp){2,}" ) {
#       // 720: permanent ; 721: temp redirect
#       return ( synth(720, regsub(req.url, "(/amp)+", "/amp") ) );
#     }

#     # see lib/devicedetect.vcl
#     call devicedetect;

#     if (
#       # mobile-(generic|iphone|android|smartphone|firefoxos|bot)
#       # mobile but not mobile-bot
#       req.http.X-UA-Device ~ "^mobile"
#       && req.http.X-UA-Device != "mobile-bot"
#       ) {

#       if (
#         req.http.Referer ~ "^https://example-with-amp\.com"
#         || req.url ~ "/amp(/|\?|$)"
#         ) {
#         # navigating on the site or yet in amp mode
#         # do nothing...
#       } else {
#         # new arrivers on mobiles
#         # redirect to amp version
#         # pagination structure: /article-uri/amp/page/2
#         if (req.url ~ "/page/") {
#           return (synth(721, regsub(req.url, "/page/", "/amp/page/")));
#         } else {
#           return (synth(721, regsub(req.url, "/?(\?|$)", "/amp\1")));
#         }
#       }
#     }
#   }
# }

include "vrocks/init.vcl";

sub vcl_recv {
  # Happens before we check if we have this in cache already.
  #
  # Typically you clean up the request here, removing cookies you don't need,
  # rewriting the request, etc.
}

sub vcl_backend_response {
  # Happens after we have read the response headers from the backend.
  #
  # Here you clean the response headers, removing silly Set-Cookie headers
  # and other mistakes your backend does.

  # Add another line of Set-Cookie in the response.
  // header.append(beresp.http.Set-Cookie, "VSESS=abbabeef");

  # CMS always set this, but doesn't really need it.
  // header.remove(beresp.http.Set-Cookie, "cxssh_status=");

}

sub vcl_deliver {
  # Happens when we have all the pieces we need, and are about to send the
  # response to the client.
  #
  # You can do accounting or modifying the final object here.
}

include "vrocks/main.vcl";

include "vrocks/terminate.vcl";
