

import std;

include "vrocks/lib/cache-control-cleanup.vcl";
include "vrocks/lib/stale-if-error.terminate.vcl";
include "vrocks/lib/debug.terminate.vcl";

sub vcl_deliver {
  # Called before a cached object is delivered to the client.

  # Remove some headers: PHP version
  unset resp.http.X-Powered-By;

  # Remove some headers: Apache version & OS
  unset resp.http.Server;
  unset resp.http.X-Varnish;
  unset resp.http.Via;
  unset resp.http.X-Generator;

  unset resp.http.Etag;

  # unset resp.http.Link;
}

# keeping it as safeguard : we don't want 5xx errors to override cache
sub vcl_backend_response {
  if (
    ! bereq.is_bgfetch    &&
    beresp.status >= 500  &&
    beresp.status < 600
    # ! bereq.uncacheable
    // && bereq.is_bgfetch
  )
  {
    # Mark as "Hit-For-Miss" 0s
    # (on pages returning error, we want requests to be queued)
    set beresp.ttl = 0s;
    set beresp.uncacheable = true;

    return (deliver);

    # return (abandon);
  }
}

# now we will avoid calling builtin

sub vcl_hash {
  return (lookup);
}

sub vcl_miss {
  # Called after a cache lookup if the requested document was not found in the cache. Its purpose
  # is to decide whether or not to attempt to retrieve the document from the backend, and which
  # backend to use.
  return (fetch);
}

sub vcl_backend_fetch {
  return (fetch);
}

sub vcl_fini {
  # Called when VCL is discarded only after all requests have exited the VCL.
  # Typically used to clean up VMODs.
  return (ok);
}

# sub vcl_backend_response {
#   return (deliver);
# }

# builtin

# sub vcl_backend_response {
#   if (bereq.uncacheable) {
#     return (deliver);
#   } else if (beresp.ttl <= 0s ||
#     beresp.http.Set-Cookie ||
#     beresp.http.Surrogate-control ~ "(?i)no-store" ||
#     (!beresp.http.Surrogate-Control &&
#     beresp.http.Cache-Control ~ "(?i:no-cache|no-store|private)") ||
#     beresp.http.Vary == "*") {
#     # Mark as "Hit-For-Miss" for the next 2 minutes
#     set beresp.ttl = 120s;
#     set beresp.uncacheable = true;
#   }
#   return (deliver);
# }
