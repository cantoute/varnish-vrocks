import header;

sub req_cleanup {
  # Happens before we check if we have this in cache already.
  #
  # Typically you clean up the request here, removing cookies you don't need,
  # rewriting the request, etc.

  # Some generic URL manipulation, useful for all templates that follow
  # First remove the facebook fbclid or Google Analytics added parameters, useless for our backend
  
  # set req.url = regsuball(req.url,
  #   "(?:(\?)|&)(?:fbclid|utm_source|utm_medium|utm_campaign|utm_content|gclid|cx|ie|cof|siteurl)=(?:[A-z0-9_\-\.%25]+)",
  #   "\1");

  # should not be needed, at worse it can end up with url?&var=value
  # that I believe to be still valid and this case only occurs when vars are appended to those
  # and usually it's those one that are appended
  # replace multiple & in query string (after '?')
  # set req.url = regsub(req.url, "(\?.*)&+", "\1&");

  # Strip trailing ? or &
  // set req.url = regsub(req.url, "[?&]+$", "");

  # Normalize the query arguments
  # set req.url = std.querysort(req.url);
  # wordpress gets lost if we change order in query string,
  # in case function is called before wordpress rules
  if (
    req.http.X-VR-Query-String-Sort &&
    req.method ~ "^GET|HEAD$"       &&
    ! req.url ~ "/wp-admin/"
  )
  {
    set req.url = std.querysort(req.url);
  }

  
  # an other approche
  if ( req.url ~ "\?" )
  {
    # extract query string and replace ? by a &
    set req.http.X-VR-qs = regsub(req.url, "^.*?\?(.*)$", "&\1");

    # remove not needed vars for backend
    set req.http.X-VR-qs =
      regsuball(req.http.X-VR-qs,
        "&(?:fbclid|utm_(?:source|medium|campaign|content)|gclid|cx|ie|cof|siteurl)=(?:[A-z0-9_\-\.%25]+)",
        ""
      );

    set req.url = regsub(req.url, "\?.*$", regsub(req.http.X-VR-QS, "^&", "?"));

    if ( req.http.X-VR-Debug )
    { }

    // just in case
    set req.url = regsub(req.url, "[\?&]+$", "");
    // set req.url = regsub(req.url, "(\?&|\?|&)$", "");


    unset req.http.X-VR-QS;
  }

  if ( req.http.Cookie )
  {
    # Remove the “has_js” cookie
    #set req.http.Cookie = regsub(req.http.Cookie, "(:?^|; )has_js=[^;]*", "");
    header.remove(req.http.Cookie, "^(has_js|is_unique)=");

    # Remove DoubleClick offensive cookies
  #   set req.http.Cookie = regsuball(req.http.Cookie, "(^|; )__gads=[^;]+(; )?", "\1");
    # Remove the Quant Capital cookies (added by some plugin, all __qca)
  #   set req.http.Cookie = regsuball(req.http.Cookie, "(^|; )__qc.=[^;]+(; )?", "\1");
    # Remove the AddThis cookies
  #   set req.http.Cookie = regsuball(req.http.Cookie, "(^|; )__atuv.=[^;]+(; )?", "\1");
    # Remove any Google Analytics based cookies
  #   set req.http.Cookie = regsuball(req.http.Cookie, "(^|; )__utm.=[^;]+(; )?", "\1");
  #   set req.http.Cookie = regsuball(req.http.Cookie, "(^|; )_ga=[^;]+(; )?", "\1");
  #   set req.http.Cookie = regsuball(req.http.Cookie, "(^|; )_gat=[^;]+(; )?", "\1");

    # (replaces all lines above)
    # Remove has_js and CloudFlare/Google Analytics __* cookies and stat counter is_unique
    # set req.http.Cookie =
    #   regsuball(req.http.Cookie,
    #     "(?:^|; )(?:__gads|__qc.|__utm.|__atuv.|_gat?|has_js|is_unique)=[^;]*",
    #     ""
    #   );
    header.remove(req.http.Cookie, "^(__gads|__qc.|__utm.|__atuv.|_gat?)=");

    # set req.http.Cookie =
    #   regsuball(req.http.Cookie,
    #     "(?:^|; )utmc(?:tr|md.|cn.)=[^;]*",
    #     ""
    #   );
    header.remove(req.http.Cookie, "^utmc(tr|md.|cn.)=");

    # set req.http.Cookie = regsuball(req.http.Cookie, "(^|; )utmcmd.=[^;]+(; )?", "\1");
    # set req.http.Cookie = regsuball(req.http.Cookie, "(^|; )utmccn.=[^;]+(; )?", "\1");

    # trying to get rid of all Cookies - for wordpress plugins for example
    if ( req.http.X-VR-Clear-Cookie-All )
    {
      unset req.http.Cookie;
    }
    elseif ( req.http.X-VR-Clear-Cookie-PHPSESSID )
    {
      header.remove(req.http.Cookie, "^PHPSESSID=");
    }

    # is Cookie empty?
    if ( req.http.Cookie ~ "^[;\s]*$" )
    {
      unset req.http.Cookie;
    }
  }
}
