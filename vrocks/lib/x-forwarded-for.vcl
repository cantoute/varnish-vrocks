
# This should generally be loaded first to make sure that the headers
# get set appropriately for all requests.  Note that when using this
# you MUST NOT fall through to the VCL default handler for vcl_recv
# since that will run the code again, resulting in the client.ip
# being added twice.

sub vcl_recv {
  # X-Forwarded-For
  if ( req.restarts == 0 )
  {
    # protect X-Forwarded-Proto
    if (
      client.ip !~ upstream_proxy ||
      ! req.http.X-Forwarded-Proto
    )
    {
      set req.http.X-Forwarded-Host = req.http.host;
      set req.http.X-Forwarded-Proto = "http";
      set req.http.X-Forwarded-Port = "80";
      set req.http.X-Forwarded-SSL = "off";
    }

    if ( client.ip ~ upstream_proxy )
    {
      # this shold be set by default
      if ( !req.http.X-Forwarded-For) {
        set req.http.X-Forwarded-For = "" + client.ip;
      }
    }
    else
    {
      # keeping it as a safeguard
      if ( req.http.X-Forwarded-For )
      {
        set req.http.X-Forwarded-For-Untrusted = req.http.X-Forwarded-For;
      }
      set req.http.X-Forwarded-For = "" + client.ip;

      if ( req.http.X-Real-IP )
      {
        set req.http.X-Real-IP-Untrusted = req.http.X-Real-IP;
      }
      set req.http.X-Real-IP = "" + client.ip;
    }
  }
}
