# bigfiles.vcl -- Bypass Cache for Large Files

sub vcl_backend_response {
  # Large static files are delivered directly to the end-user without
  # waiting for Varnish to fully read the file first.
  # Varnish 4 fully supports Streaming, so use streaming here to avoid locking.
  # In 6.x default do_stream = true
  if ( std.integer(beresp.http.Content-Length, 0) > 0 )
  {
    set beresp.do_stream = true;
    # Check memory usage it'll grow in fetch_chunksize blocks (128k by default) if the backend doesn't send a Content-Length header, so only enable it for big objects
  }
  // else
  // {
  //   set beresp.do_stream = false;
  // }

  # don't cache files > 5 MB
  if ( std.integer(beresp.http.Content-Length, 0) > 1048576 * 5 )
  {
    set beresp.uncacheable = true;

    if ( bereq.http.X-VR-Debug )
    {
      set beresp.http.X-VR-Debug-Msg = beresp.http.X-VR-Debug-Msg + ", no-cache: file bigger then 5M";
    }

    return (deliver);
  }
}
