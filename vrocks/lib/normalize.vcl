
sub vcl_recv {
  # Happens before we check if we have this in cache already.
  #
  # Typically you clean up the request here, removing cookies you don't need,
  # rewriting the request, etc.

  # Only deal with "normal" types
  if (
    req.method != "GET"
    && req.method != "HEAD"
    && req.method != "PUT"
    && req.method != "POST"
    && req.method != "TRACE"
    && req.method != "OPTIONS"
    && req.method != "PATCH"
    && req.method != "DELETE"
    && req.method != "PROPFIND" # WebDAV
  )
  {
    /* Non-RFC2616 or CONNECT which is weird. */
    /* Better not send the packet upstream, when using a non-valid HTTP method */
    return (synth(400, "Non-valid HTTP method!"));
  }

  if ( !req.http.Host )
    { return (synth(400, "Need a host header.")); }

  # Remove the proxy header (see https://httpoxy.org/#mitigate-varnish)
  unset req.http.proxy;

  # double quotes not allowed in url (still permitting them in query string)
  # credit https://stackoverflow.com/questions/43983671/how-to-escape-a-double-quote-in-varnish-vcl
  # if (req.url ~ "^/[^\?]*[\x27<>()\x22]")
  # {
  #   return (synth(403, "Forbidden"));
  # }

  # Normalize host header
  set req.http.Host = std.tolower(req.http.Host);

  # Normalize the header, remove the port (in case you're testing this on various TCP ports)
  set req.http.Host = regsub(req.http.Host, ":[0-9]+", "");

  # Strip hash, backend doesn't need it.
  set req.url = regsub(req.url, "\#.*$", "");

  # remove empty query string
  set req.url = regsub(req.url, "\?+&*$", "");

  # Normalize Accept-Encoding header and compression
  # https://www.varnish-cache.org/docs/3.0/tutorial/vary.html
  # note that nginx as front proxy we cache it all with gzip encoding
  # by setting in nginx
  # gzip_proxied any;
  # gunzip on; # this will gunzip compressed response to client that don't support it
  if ( req.http.Accept-Encoding )
  {
    # this is dealt by nginx frontend
    # if ( req.http.User-Agent ~ "MSIE 6") {
    #   unset req.http.Accept-Encoding;
    # } else
    if ( req.url ~ "(?i)\.(?:jpe?g|png|gif|bmp|web[mp]|ico|t?[gbx]z|bz2?|zip|rar|mp[34]|ogg|mpeg4?|avi|mkv|bin|iso)(?:\?|$)")
    {
      # Do no compress compressed files...
      unset req.http.Accept-Encoding;
    }
    elseif ( req.http.Accept-Encoding ~ "gzip" )
    {
      set req.http.Accept-Encoding = "gzip";
    }
    elseif ( req.http.Accept-Encoding ~ "deflate" )
    {
      set req.http.Accept-Encoding = "deflate";
    }
    else
    {
      unset req.http.Accept-Encoding;
    }
  }

  # Implementing websocket support (https://www.varnish-cache.org/docs/4.0/users-guide/vcl-example-websockets.html)
  if (req.http.Upgrade ~ "(?i)websocket")
  {
    return (pipe);
  }
}

sub vcl_recv {

  
  if (
    req.method ~ "^GET|HEAD$" &&
    req.url ~ "^[^?]*/{2,}"
  )
  {
    # fix double slash // in url
    return ( 
      synth(
        720,
        regsuball(
          req.url,
          # catch //+ keeping query string safe
          "^(?:([^?]*?/)/+)",
          "\1"
        )
      )
    );
  }
}

sub vcl_pipe {
  # Called upon entering pipe mode.
  # In this mode, the request is passed on to the backend, and any further data from both the client
  # and backend is passed on unaltered until either end closes the connection. Basically, Varnish will
  # degrade into a simple TCP proxy, shuffling bytes back and forth. For a connection in pipe mode,
  # no other VCL subroutine will ever get called after vcl_pipe.

  # Note that only the first request to the backend will have
  # X-Forwarded-For set.  If you use X-Forwarded-For and want to
  # have it set for all requests, make sure to have:
  # set bereq.http.connection = "close";
  # here.  It is not set by default as it might break some broken web
  # applications, like IIS with NTLM authentication.

  # http://www.varnish-cache.org/ticket/451
  # This forces every pipe request to be the first one.
  set bereq.http.Connection = "close";

  # Implementing websocket support (https://www.varnish-cache.org/docs/4.0/users-guide/vcl-example-websockets.html)
  if ( req.http.Upgrade )
  {
    set bereq.http.Upgrade = req.http.Upgrade;
  }

  # return (pipe);
}


sub vcl_backend_response {
  # Happens after we have read the response headers from the backend.
  #
  # Here you clean the response headers, removing silly Set-Cookie headers
  # and other mistakes your backend does.

  # Sometimes, a 301 or 302 redirect formed via Apache's mod_rewrite can mess with the HTTP port that is being passed along.
  # This often happens with simple rewrite rules in a scenario where Varnish runs on :80 and Apache on :8080 on the same box.
  # A redirect can then often redirect the end-user to a URL on :8080, where it should be :80.
  # This may need fine-tuning on your setup.
  #
  # To prevent accidental replace, we only filter the 301/302/303 redirects for now.
  if (
    beresp.http.Location
#    beresp.status == 301 # 301 Permanent Redirect
#    || beresp.status == 302 # 302 Temporary Redirect
#    || beresp.status == 303 # 303 See Other (forces PUT|POST into GET)
#    || beresp.status == 307 # 307 Temporary Redirect
  )
  {
    set beresp.http.Location =
      regsub(
        beresp.http.Location,
        ":[0-9]+", ""
      );
  }

  # is Set-Cookie empty?
  if ( beresp.http.Set-Cookie ~ "^[; ]*$" )
  {
    unset beresp.http.Set-Cookie;
  }
}

# Varnish 6 - Additionally, you might want to stop cache insertion when a backend fetch returns an 5xx error:
# sub vcl_backend_response {
#   if (beresp.status >= 500 && bereq.is_bgfetch) {
#     return (abandon);
#   }
# }
