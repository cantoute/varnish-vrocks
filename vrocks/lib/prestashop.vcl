sub vcl_recv {

    if ( req.http.X-VR-Rule-Prestashop )
    {
        #we should not cache any page for sales
        if ( req.url ~ "/(?:cart|order|addresses|order-(?:detail|confirmation|return))\.php" )
        {
            return (pass);
        }
    }
}