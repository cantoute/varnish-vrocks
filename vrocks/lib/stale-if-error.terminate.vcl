# If Stale-If-Error is activated and object is cacheable
# merde 
sub vcl_backend_response {
  if (
    bereq.http.X-VR-Stale-If-Error  &&
    beresp.ttl > 0s                 &&
    beresp.status < 500             &&
    ! bereq.uncacheable             &&
    ! beresp.uncacheable
  )
  {
    set beresp.grace = beresp.grace + 10d;

    # TODO: check meaning of keep in latest versions of varnish
    # in 6.x seems it ment keeping obj headers for 304 response only
    set beresp.keep = 10d;
    
    if ( bereq.http.X-VR-Debug )
    {
      set beresp.http.X-VR-Debug-Msg = beresp.http.X-VR-Debug-Msg + ", beresp.grace: " + beresp.grace;
      set beresp.http.X-VR-Debug-Msg = beresp.http.X-VR-Debug-Msg + ", beresp.keep: " + beresp.keep;
    }
  }
  else
  {
    if ( bereq.http.X-VR-Debug )
    {
      set beresp.http.X-VR-Debug-Msg = beresp.http.X-VR-Debug-Msg + ", not adding grace: not cacheable";
      set beresp.http.X-VR-Debug-Msg = beresp.http.X-VR-Debug-Msg + ", bereq.uncacheable:" + bereq.uncacheable + " beresp.ttl:" + beresp.ttl + " beresp.uncacheable:" + beresp.uncacheable;
    }
  }
}