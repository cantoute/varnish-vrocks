
sub vcl_recv {
  # Ignore Cache-Control from requests via proxy caches and badly behaved crawlers
  # like M$ bots that send no-cache with every request.
  if (
    req.http.X-VR-Force-Reload-Allow  &&
    std.healthy(req.backend_hint)     &&
    req.restarts == 0                 &&
    req.method ~ "^GET|HEAD$"
  )
  {
    if (
      req.http.Via                      ||
      req.http.X-UA-Device == "bot"     ||
      req.http.User-Agent ~
        "(?i)bot|spider|search|msie|track|slurp|archive|facebookexternalhit|scan|ping" ||
      req.http.User-Agent ~ "(?i)curl|wget|siege|fetch|bench|java"
    )
    {
      unset req.http.Cache-Control;
      unset req.http.Pragma;
    }

    # force a backend fetch on client "reload"
    # Click reload => Cache-Control: max-age=0
    # shift click => Cache-Control: no-cache
    #
    # seems that with either Pragma|Cache-Control == no-cache
    # varnish doesn't cache the new object.
    # seen it returning a no-cache header to ajax requests that should
    # or this could be a firefox bug
    # have cacheable headers. Un-setting them seems to have no effect :-/
    if (
      # shift+reload
      req.http.Cache-Control ~ "no-cache"
      # (
      #   req.http.Cache-Control ~ "no-cache"

      #   # alternative: intercept reload
      #   # || (
      #   #   req.http.Cache-Control ~ "max-age=0"
      #   #   # disabling it for ajax requests
      #   #   # some clients send max-age=0 headers
      #   #   # when it should use ifModifiedSince
      #   #   # Safari?
      #   #   && req.http.X-Requested-With != "XMLHttpRequest"
      #   # )
      # ) &&
    )
    {
      # see init.vcl
      # if something else triggered a force-grace, it'ss have priority
      set req.http.X-VR-Force-Miss = "true";
      #set req.hash_always_miss = true;

      if ( req.http.X-VR-Debug )
      {
        set req.http.X-VR-Debug-Msg = req.http.X-VR-Debug-Msg + ", req fresh fetch (" + req.http.Cache-Control + ") - X-Real-IP:" + req.http.X-Real-IP;
      }

      # return (restart);
    }
  }
}
