
sub vcl_recv {
  # Send Surrogate-Capability headers to announce ESI support to backend
  set req.http.Surrogate-Capability = "key=ESI/1.0";
}

sub vcl_backend_response {
  # Pause ESI request and remove Surrogate-Control header
  if ( beresp.http.Surrogate-Control ~ "ESI/1.0" )
  {
    unset beresp.http.Surrogate-Control;
    set beresp.do_esi = true;
  }
}
