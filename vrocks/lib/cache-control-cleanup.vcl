
# remove reverse proxy specific Cache-Control directives
import header;

sub vcl_backend_response {
  if(
    bereq.http.X-VR-Clean-Cache-Control &&
    beresp.http.Cache-Control
  )
  {
    if(bereq.http.X-VR-Debug)
    {
      set beresp.http.X-Debug-Cache-Control = beresp.http.Cache-Control;
    }

    # remove cache headers that are specific to reverse proxy caching

    set beresp.http.Cache-Control =
      regsuball(beresp.http.Cache-Control,
        "(?:^|, *)(?:s-maxage|stale-while-revalidate|stale-if-error)=[^,]*",
        ""
      );
    set beresp.http.Cache-Control =
      regsuball(beresp.http.Cache-Control,
        "(?:, *)+",
        ", "
      );
    set beresp.http.Cache-Control =
      regsub(beresp.http.Cache-Control,
        "^(?:, *)+|(?:, *)+$",
        ""
      );
  
    # header.remove(beresp.http.Cache-Control, "^(?:s-maxage|stale-while-revalidate|stale-if-error)=");

    if(beresp.http.Cache-Control ~ "^[, ]*$")
    {
      unset beresp.http.Cache-Control;
    }
  }
}
