# static.vcl -- Static File Caching for Varnish

sub vcl_recv {
  if( req.url ~ "^/\.well-known/" )
  {
    return (pass);
  }

  # caching robots.txt or sitemap*.xml usually is a pain
  if (
    req.url ~ "^/sitemap[^\?]*\.xml"  ||
    req.url ~ "^/robots\.txt"
  )
  {
    # uncacheable
    return (pass);
  }

  if (
    req.url ~ "(?i)^/[^?]+\.(?:css|js|html?|xml|xhtml|shtml|json|svgz?)(?:\?|$)" ||
    req.url ~ "(?i)^/[^?]+\.(?:php[1-9]?|phar|aspx?|cfm|do|jspa?|java)(?:\?|$)"
  )
  {
    # allow cache (js, css, html, xml, xhtml, json, svg)
    # but depending on nginx conf, chances we never see those requests
  }
  elseif ( req.url ~ "^/[^\?\#]*\." )
  {
    # a dot in the uri (still allowing dots in query string)
    return (pass);
  }
}

# if (
#   req.url ~
#     "(?i)^/[^?]+\.(?:"                                +
#     "zip|gz|7z|tar|t[bgx]z|bz2|xz|rar|iso|dmg|bin"    +
#     "|pdf|rtf|docx?|xlsx?|pptx?|csv|odt|otf|eot|less" +
#     "|mov|mp[234]|mpe?g|avi|mk[av]|swf|flv|web[am]"   +
#     "|wav|flac|ogg|ogm|opus|md|ttf|woff2?"            +
#     "|jpe?g|png|gif|web[ap]|svgz?|ico|bmp"            +
#     "|exe|com|[zc]?sh|bash|[a-z]?sql"                 +
#     "|map|ts|cs|phps|java|tmp|scss|sass"              +
#     ")(?:\?|$)"
# )
# {
#   return (pass);
# }

# if (
#   req.url ~ "(?i)^/[^?]+\.(?:jpe?g|png|gif|web[ap]|svgz?|ico|bmp)(?:\?|$)"
# )
# {
#   return (pass);
# }



# if (
#   req.url ~ "(?i)^/[^?]+\.(?:xml|txt)(?:\?|$)"
# )
# {
#   return (pass);
# }

# Caching those files can help site keeping online in disaster
# if (
#   req.url ~ "(?i)^/[^?]+\.(?:css|js|x?html?|json)(?:\?|$)"
# )
# {
#   return (pass);
# }
