sub vcl_hit {
  set req.http.X-VR-Obj-Hit = "true";

  if ( ! std.healthy(req.backend_hint) )
  {
    set req.http.X-VR-Debug-Msg =  req.http.X-VR-Debug-Msg + ", hit(BackendSick)";
    
    return (deliver);
  }

  # deliver anything we've got regardless of age
  if ( req.http.X-VR-Force-Grace )
  {
    if ( req.http.X-VR-Debug ) {
      set req.http.X-VR-Debug-Msg = req.http.X-VR-Debug-Msg + ", hit(ForceGrace)";
    }

    return (deliver);
  }

  if (req.http.X-VR-Stale-If-Error) {
    # Stale-If-Bot
    if (
      req.http.X-VR-Stale-If-Bot &&
      (
        req.http.X-UA-Device == "bot" ||
        req.http.User-Agent ~
          "(?i)bot|spider|search|msie|track|slurp|archive|facebookexternalhit|scan|ping" ||
        req.http.User-Agent ~ "(?i)curl|wget|siege|fetch|bench|java"
      ) &&
      ! (
        # respect normal grace for those urls
        # req.url ~ "/(?:feed|rss|atom)/?(?:$|\?)"  ||
        # docs with .txt .xml respect normal grace
        # req.url ~ "^/[^\?]+\.(?:txt|xml|rss)(?:$|\?)"
        req.url ~ "txt|rss|xml|feed|atom"
      ) &&
      (
        # we extend grace (default 1d)
        # bots get staled object limiting to 1d old
        # object will be updated in background
        obj.ttl + obj.grace >
          10d - std.duration(req.http.X-VR-Stale-If-Bot, 1d)
      )
    )
    {
      return (deliver);
    }

    # respect normal stale in normal conditions
    # for stale-if-error we artificially extended grace 10d
    # to have original grace period, in normal conditions,
    # we have to subtract 10d to 
    if (
      obj.ttl + obj.grace < 10d
    )
    {
      // removed in 7.0
      // return (miss);

      // workaround
      set req.http.X-VR-Force-Miss = "true";

      return (restart);
    }
  } # if req.http.X-VR-Stale-If-Error
}

sub vr_stale_if_error {
  # as safeguard, should be caught in backend-error.vcl
  if ( bereq.is_bgfetch )
  {
    # not passing by vcl_synth
    return (abandon);
  }
  
  if (
    bereq.http.X-VR-Stale-If-Error  &&
    bereq.http.X-VR-Obj-Hit         &&
    ! bereq.uncacheable             &&
    bereq.method ~ "GET|HEAD"
  )
  {
    # passing to vcl_synth status 503 if not is_bgfetch
    return (abandon);
  }

  # if (
  #   bereq.http.X-VR-Retry-If-Error &&
  #   bereq.retries < 2 &&
  #   # don't retry on shift+reload
  #   ! bereq.http.Cache-Control ~ "no-cache"
  # )
  # {
  #   return (retry);
  # }
}

sub vcl_backend_response {
  if (
    beresp.status >= 500 &&
    beresp.status < 600
  )
  {
    call vr_stale_if_error;
  }
}

sub vcl_backend_error {
  call vr_stale_if_error;
}

sub vcl_synth {
  if (
    req.http.X-VR-Stale-If-Error    &&
    # abandon => 503
    resp.status == 503              &&
    # resp.status >= 500              &&
    # resp.status <  600              &&
    req.http.X-VR-Obj-Hit           &&
    # we are never too sure
    ! req.http.X-VR-Force-Grace
  )
  {
    set req.http.X-VR-Force-Grace = "true";

    # Restart processing of the request. You can restart the processing of the whole transaction.
    # Changes to the req object are retained.
    return (restart);
  }
}

# example varnishplus implementation
# import stale;
# import headerplus;

# sub stale_if_error {
#     if (beresp.status >= 500 && stale.exists()) {
#         # Tune this value to match your traffic and caching patterns
#         stale.revive(20m, 1h);
#         stale.deliver();
#         return (abandon);
#     }
# }

# sub vcl_backend_response {
#     call stale_if_error;
# }

# sub vcl_backend_error {
#     call stale_if_error;
# }

# sub vcl_backend_response {
#     Automatically set the stale-if-error value (keep) from Cache-Control
#     headerplus.init(beresp);
#     set beresp.keep = std.duration(
#             headerplus.attr_get("Cache-Control", "stale-if-error") + "s",
#             10d); # This is the default value if not found
#     # set beresp.keep = 10d;
# }
