

sub vcl_recv {
  # Only cache GET or HEAD requests. This makes sure the POST requests are always passed.
  if ( ! req.method ~ "^GET|HEAD$" )
  {
    return (pass);
  }

  if (
    req.http.Authorization  ||
    req.http.Authenticate
  )
  {
    # Not cacheable by default
    return (pass);
  }

  if ( req.url ~ "(?i)no-?cache" )
  {
    # Not cacheable
    return (pass);
  }

  # passthrough
  if ( req.url ~ "^/\.well-known/" )
  {
    return (pass);
  }

  # AJAX requests
  if (
    req.http.X-Requested-With == "XMLHttpRequest" &&
    ! req.http.X-VR-Cache-Ajax
  )
  {
    return (pass);
  }

  if (
    req.url ~ "^/robots\.txt"       ||
    req.url ~ "^/(app-)?ads\.txt"   ||
    req.url ~ "^/pubvendors\.json"
    # req.url ~ "^/sitemap.*\.xml"
  )
  {
    return (pass);
  }
}

sub vcl_backend_response {
  if ( ! bereq.uncacheable )
  {
    # only cache status ok
    if (
      beresp.status == 404
      || beresp.status == 410
    )
    {
      # set beresp.grace = 0s;

      # dont cache too long
      # if ( beresp.ttl > 120s ) {
      #   set beresp.ttl = 120s;
      # }
      
      if ( beresp.ttl < 2s )
      {
        # avoid a mistake taking down server
        set beresp.ttl = 2s;
      }
    }
    elseif (
      ! (
        beresp.status == 200 ||
        beresp.status == 301
        # see Note in footer
        # Varnish amends beresp before calling vcl_backend_response
        # ... that was theory. TODO: check this
        # || beresp.status == 304
      )
    )
    {
      if (bereq.http.X-VR-Debug )
      {
        set beresp.http.X-VR-Debug-Msg = beresp.http.X-VR-Debug-Msg + ", response status not cacheable";
      }

      set beresp.uncacheable = true;
    }

    # if ( beresp.http.Cache-Control ~ "private" ) {
    #   set beresp.uncacheable = true;
    # }

    # if ( beresp.ttl <= 0s ) {
    #   set beresp.uncacheable = true;
    # }

    # if ( beresp.http.Vary == "*" ) {
    #   set beresp.uncacheable = true;
    # }

    # dont cache empty response(200) too long
    # status 3xx can be empty!

    # TODO: perhaps this should be checked in stale-if-error or in backend-error
    if (
      bereq.method == "GET"   &&
      beresp.ttl > 0s         &&
      beresp.status == 200    &&
      (
        // beresp.http.Content-Length
        // && std.integer(beresp.http.Content-Length, 0) == 0
        beresp.http.Content-Length == "0"
      )
    )
    {
      if ( bereq.is_bgfetch ) {
        return (abandon);
      }

      set beresp.uncacheable = true;

      # it was marked cacheable but it's empty
      # so keep a short cache just to avoid a broken script taking down backend
      # set beresp.ttl = 10s;
      # set beresp.grace = 0s;

      if ( bereq.http.X-VR-Debug )
      {
        set beresp.http.X-VR-Debug-Msg = beresp.http.X-VR-Debug-Msg + ", warning Content-Length=0";
      }
    }
  }
}

# from builtin vcl
sub vcl_backend_response {
  if (
    bereq.uncacheable ||
    beresp.uncacheable
  ) {
    # return (deliver);
  }
  elseif (
    beresp.ttl <= 0s                                  ||
    # beresp.http.Set-Cookie                          ||
    beresp.http.Surrogate-control ~ "(?i)no-store"    ||
    (
      ! beresp.http.Surrogate-control &&
      beresp.http.Cache-Control ~ "(?i:no-cache|no-store|private)"
    )                                                 ||
    beresp.http.Vary ~ "\*" 
  )
  {
    # Mark as "Hit-For-Miss" for the next 2 minutes
    set beresp.ttl = 120s;
    set beresp.uncacheable = true;
  }

  # has to come before builtin
  if (
    bereq.http.X-VR-Clear-Cookie-All  &&
    ! beresp.uncacheable              &&
    beresp.http.Set-Cookie          
  )
  {
    unset beresp.http.Set-Cookie;
  }

  if (
    beresp.http.Set-Cookie          
  )
  {
    # Mark as "Hit-For-Miss" for the next 2 minutes
    set beresp.ttl = 120s;
    set beresp.uncacheable = true;
  }
}

# Note about status 304
# https://fr.slideshare.net/gogcomdev/indepth-caching-in-varnish-gog-varnish-meetup-march-2019
# 16. Not modified If a cache object is being refreshed and backend returns a 304 response,
# Varnish amends beresp before calling vcl_backend_response:
# ● If the gzip status changed, Content-Encoding is unset and any Etag is weakened
# ● Any headers not present in the 304 response are copied from the existing cache object
# ● The status gets set to 200
