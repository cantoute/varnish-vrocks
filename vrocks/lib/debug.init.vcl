
sub vcl_recv {
	unset req.http.X-Cache;
}

sub vcl_hit {
	set req.http.X-Cache = "hit";
}

sub vcl_miss {
	set req.http.X-Cache = "miss";
}

sub vcl_pass {
	set req.http.X-Cache = "pass";
}

sub vcl_pipe {
	set req.http.X-Cache = "pipe uncacheable";
}

sub vcl_synth {
	set resp.http.X-Cache = "synth uncacheable";
}

sub vcl_hit {
  if ( req.http.X-VR-Debug ) {
    set req.http.X-VR-Debug-Msg = req.http.X-VR-Debug-Msg + ", Obj-Hit(ttl:" + obj.ttl + ")";
  }
}

sub vcl_deliver {
	if (obj.uncacheable) {
		set req.http.X-Cache = req.http.X-Cache + " uncacheable" ;
	} else {
		set req.http.X-Cache = req.http.X-Cache + " cached" ;
	}
	# uncomment the following line to show the information in the response
	set resp.http.X-Cache = req.http.X-Cache;
}

# sub vcl_deliver {
#   if ( req.http.X-VR-Debug ) {
#     # SaintMode trouble count
#     set resp.http.X-VR-Debug-Trouble-Count = "sm1:" + sm1.blacklist_count() + " sm2:" + sm2.blacklist_count();
#   }
# }

sub vcl_recv {
  if ( req.http.X-VR-Debug )
  {
    if ( req.restarts > 0 )
    {
      set req.http.X-VR-Debug-Msg = req.http.X-VR-Debug-Msg + ", req.restarts=" + req.restarts;
    }

    if ( !std.healthy(req.backend_hint) )
    {
      set req.http.X-VR-Debug-Msg = req.http.X-VR-Debug-Msg + ", Backend Sick";
    }
  }
}

// TODO: needs re-thinking
sub vcl_hit {
  if ( req.http.X-VR-Debug )
  {
    if ( obj.ttl > 0s )
    {
      set req.http.X-VR-Grace = "NONE";
    }
    elseif ( obj.ttl + obj.grace > 0s )
    {
      set req.http.X-VR-Grace = "GRACE";
    }
    elseif ( !std.healthy(req.backend_hint) )
    {
      set req.http.X-VR-Grace = "KEEP " + (obj.ttl + obj.keep) + " (backend sick)";
    }
    else
    {
      set req.http.X-VR-Grace = "KEEP " + (obj.ttl + obj.keep);
    }

    set req.http.X-VR-Debug-Msg = req.http.X-VR-Debug-Msg + ", vcl_hit(" + obj.ttl + ":" + obj.grace + ":" + obj.keep + ")";
  }
}
