sub vcl_backend_response {
  # Backend responded an error status.
  # Declare backend unhealthy only on cacheable request.
  if ( 
    beresp.status >= 500  &&
    beresp.status < 600   &&
    ! bereq.uncacheable
  )
  {
    set beresp.http.X-VR-Debug-Msg = beresp.http.X-VR-Debug-Msg + ", error: status:" + beresp.status + " reason:" + beresp.reason;

    set beresp.http.X-VR-Req-Cacheable = "true";

    if ( bereq.is_bgfetch )
    {
      # is_bgfetch, so at some point we did have this page in cache
      # We can presume it was a legitimate request, backend seams sick.
      saintmode.blacklist(30s);

      # is_bgfetch aren't passed to vcl_synth
      return (abandon);
    }

    if (
      # prevent bots triggering saintmode.blacklist() on stupid urls
      ! (
        bereq.http.X-UA-Device == "bot" ||
        bereq.http.User-Agent ~
          "(?i)bot|spider|search|msie|track|slurp|archive|facebookexternalhit|scan|ping" ||
        bereq.http.User-Agent ~ "(?i)curl|wget|siege|fetch|bench|java"
      ) &&
      # prevent clients bypassing cache from triggering blacklist()
      ! bereq.http.Cache-Control ~ "no-cache" # Shift-Reload
    )
    {
      saintmode.blacklist(10s);
    }
  }
}

sub vcl_backend_error {
  set beresp.http.X-VR-Debug-Msg = beresp.http.X-VR-Debug-Msg + ", error: status:" + beresp.status + " reason:" + beresp.reason;

  if (
    bereq.method ~ "GET|HEAD" &&
    bereq.retries < 1         &&
    ! bereq.is_bgfetch
  )
  {
    return (retry);
  }
 
  # Could not connect to the backend, safe to blacklist().
  saintmode.blacklist(std.duration(30s));

  if ( bereq.is_bgfetch )
  {
    return (abandon);
  }
}

# Add a refresh of 10s for client to retry
sub vcl_synth {
  if (
    resp.http.X-VR-Req-Cacheable  &&
    req.method  == "GET"          &&
    resp.status >= 500            &&
    resp.status < 600             &&
    ! resp.http.Refresh 
  )
  {
    set resp.http.Refresh  = "10";
  }

  if ( req.http.X-VR-Debug-Msg )
  {
    set resp.http.X-VR-Debug-Msg = req.http.X-VR-Debug-Msg;
  }
}

# sub vcl_backend_response {
#   if (
#     bereq.method == "GET" &&
#     ! beresp.http.Refresh &&
#     beresp.status >= 500
#   ) {
#     set beresp.http.Refresh = "10";
#   }
# }


# sub vcl_backend_error {
#   # get browsers to retry in 10s
#   if (
#     bereq.method == "GET" &&
#     ! beresp.http.Refresh
#   )
#   {
#     set beresp.http.Refresh = "10";
#   }
# //   set beresp.http.Content-Type = "text/html; charset=utf-8";
# //   set beresp.http.Retry-After = "5";

# //   synthetic( {"<!DOCTYPE html>
# // <html>
# //   <head>
# //   <title>"} + beresp.status + " " + beresp.reason + {"</title>
# //   </head>
# //   <body>
# //   <h1>Error "} + beresp.status + " " + beresp.reason + {"</h1>
# //   <p>"} + beresp.reason + {"</p>
# //   <h3>Guru Meditation:</h3>
# //   <p>XID: "} + bereq.xid + {"</p>
# //   <hr>
# //   <p>Varnish cache server</p>
# //   </body>
# // </html>"} );

# //   return (deliver);
# }
