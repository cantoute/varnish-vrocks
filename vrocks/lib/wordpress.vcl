# --- Wordpress specific configuration

import header;

sub vcl_recv {
  if ( req.http.X-VR-Rule-Wordpress ) {
    # inspired by https://www.varnish-software.com/developers/tutorials/configuring-varnish-wordpress/
    # don't cache these special pages, presuming wordpress is at site root
    if (
      req.http.Cookie ~ "wordpress_(?!test_)[a-zA-Z0-9_]+|wp-postpass|comment_author_[a-zA-Z0-9_]+"
        + "|woocommerce_cart_hash|woocommerce_items_in_cart|wp_woocommerce_session_[a-zA-Z0-9]+"
        + "|wordpress_logged_in_|comment_author|resetpass" ||
      req.url ~ "^/wp-admin" ||
      req.url ~ "^/my-account" ||
      req.url ~ "^/lost-password" ||
      req.url ~ "^/wp-(login|cron|comments-post|activate|mail|register)\.php" ||
      req.url ~ "^/bb-(login|reset-password|register)\.php" ||
      req.url ~ "^/xmlrpc\.php" ||
      req.url ~ "^/(register|control)\.php" ||
      req.url ~ "^/(wc-api|cart|checkout|addons|administrator|resetpass)" ||
      req.url ~ "(\?|&)wc-api=" ||
      req.url ~ "^/(log|sign)(in|out)" ||
      req.url ~ "(^/|\?|&)preview=" ||
      req.url ~ "(\?|&)wc-ajax=get_refreshed_fragments" ||
      req.url ~ "(\?|&)add-to-cart=[^&]+" ||
      req.url ~ "(\?|&)wordfence_lh=[^&]+" ||
      req.url ~ "edd_action"
      # french
      || req.url ~ "^/mon-compte"
    )
    {
      if ( req.http.X-VR-Debug )
      {
        set req.http.X-VR-Debug-Msg = req.http.X-VR-Debug-Msg + ", pass:Wordpress Rules";
      }

      return (pass);
    }

    # copied from https://gist.github.com/lukecav/3d42541fc7735c41729a2fd4681e5588
    # DO NOT CACHE LOGGED IN USERS and CARTS
    # ##########################################################

    ### looks like we might actually cache it!

    # fix up the request
    set req.url = regsub(req.url, "[?&]replytocom=[^&]+", "");

    # remove random string that bypass cache
    set req.url = regsub(req.url, "^(.*/feed/?)\?.*$", "\1");

    # # Remove the wp-settings-1 cookie
    # // set req.http.Cookie = regsuball(req.http.Cookie, "wp-settings-1=[^;]+(; )?", "");
    # header.remove(req.http.Cookie, "wp-settings-1=");
    
    # # Remove the wp-settings-time-1 cookie
    # // set req.http.Cookie = regsuball(req.http.Cookie, "wp-settings-time-1=[^;]+(; )?", "");
    # header.remove(req.http.Cookie, "wp-settings-time-1=");

    # # Remove the wp test cookie
    # // set req.http.Cookie = regsuball(req.http.Cookie, "wordpress_test_cookie=[^;]+(; )?", "");
    # header.remove(req.http.Cookie, "wordpress_test_cookie=");

    # # Remove xLanguage_ cookie
    # // set req.http.Cookie = regsuball(req.http.Cookie, "xLanguage_=[^;]+(; )?", "");
    # header.remove(req.http.Cookie, "xLanguage_=");

    # Remove any cookies left
    #unset req.http.Cookie;

    # return(hash);
  }
}

sub vcl_backend_response {
  if (
    bereq.http.X-VR-Rule-Wordpress &&
    ! bereq.uncacheable &&
    ! beresp.uncacheable
  )
  {
    # Remove the Set-Cookie header when a specific Wordfence cookie is set
    # obsolete check https://www.wordfence.com/help/general-data-protection-regulation/#cookies-set-by-the-wordfence-plugin
    # if (beresp.http.Set-Cookie ~ "wfvt_|wordfence_verifiedHuman") {
	  #   # unset beresp.http.Set-Cookie;
    #   header.remove(beresp.http.Set-Cookie, "wfvt_");
    #   header.remove(beresp.http.Set-Cookie, "wordfence_verifiedHuman");
	  # }

    # this cookie is set to verify the user can access the site from a country restricted by the country blocking feature.
    if( beresp.http.Set-Cookie ) {
      header.remove(beresp.http.Set-Cookie, "^wfCBLBypass=");
    }

    # unset beresp.http.Link;

    # WP Varnish Caching plugin uses this header
    if ( beresp.http.x-vc-ttl ) {
      set beresp.http.X-VR-Default-TTL = beresp.http.x-vc-ttl;
      unset beresp.http.x-vc-ttl;
      unset beresp.http.x-vc-enabled;
    }
  }
}
