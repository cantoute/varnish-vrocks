

# sub vcl_backend_response {

#   if ( bereq.http.X-VR-Debug ) {

#     if ( ! bereq.http.X-VR-Enabled ) {

#       set beresp.http.X-VR-Cacheable = "NO: Disabled";

#     } elseif ( bereq.uncacheable ) {

#       if ( bereq.http.Cookie ~ "PHPSESSID" ) {
#         set beresp.http.X-VR-Cacheable = "NO: Not Cacheable Request - PHPSESSID";
#       } else {
#         set beresp.http.X-VR-Cacheable = "NO: Not Cacheable Request";
#       }

#     } elseif ( beresp.uncacheable ) {

#       if ( beresp.http.Cache-Control ~ "private" ) {
#         set beresp.http.X-VR-Cacheable = "NO: Cache-Control = private";
#       } else {
#         set beresp.http.X-VR-Cacheable = "NO: Not Cacheable Response";
#       }

#     } elseif ( beresp.ttl <= 0s ) {

#       set beresp.http.X-VR-Cacheable = "NO: beresp.ttl=0";

#     } elseif ( bereq.http.X-VR-Debug ) {
#       # Varnish determined the object was cacheable
#       set beresp.http.X-VR-Cacheable = "YES: "
#         + beresp.ttl
#         + ":" + beresp.grace
#         + ":" + beresp.keep;
#     }


#     if ( beresp.do_stream ) {
#       set beresp.http.X-VR-Stream = "YES";
#     }
#   }
# }


sub vcl_backend_response {
  if ( ! bereq.http.X-VR-Enabled )
  {
    if ( bereq.http.X-VR-Debug )
    {
      set beresp.http.X-VR-Cacheable = "NO: Disabled";
    }
  }
  else
  {
    if ( bereq.http.X-VR-Debug )
    {
      # Varnish is enabled and we want debug info
      if ( bereq.uncacheable )
      {
        if ( bereq.http.Cookie ~ "PHPSESSID" )
        {
          set beresp.http.X-VR-Cacheable = "NO: Not Cacheable Request - PHPSESSID";
        } 
        elseif ( bereq.http.Cookie )
        {
          set beresp.http.X-VR-Cacheable = "NO: Cookie in request";
        }
        else
        {
          set beresp.http.X-VR-Cacheable = "NO: Not Cacheable Request";
        }
      }
      elseif ( beresp.uncacheable )
      {
        if ( beresp.http.Vary == "*" )
        {
          set beresp.http.X-VR-Cacheable = "NO: Vary: *";
        }
        elseif ( beresp.http.Surrogate-control ~ "(?i)no-store" )
        {
          set beresp.http.X-VR-Cacheable = "NO: Surrogate-control: "
                  + beresp.http.Surrogate-control;
        }
        elseif ( beresp.http.Cache-Control ~ "(?i:no-cache|no-store|private)" )
        {
          set beresp.http.X-VR-Cacheable = "NO: Cache-Control: "
                  + beresp.http.Cache-Control;
        }
        else
        {
          set beresp.http.X-VR-Cacheable = "NO: resp.uncacheable";
        }
      }
      elseif ( beresp.ttl <= 0s )
      {
        set beresp.http.X-VR-Cacheable = "NO: Error: beresp.ttl=0 and !beresp.uncacheable";
      }
      else
      {
        # Varnish determined the object was cacheable
        set beresp.http.X-VR-Cacheable = "YES";
      }

      if ( beresp.do_stream )
      {
        set beresp.http.X-VR-Stream = "YES";
      }

      if ( beresp.do_esi )
      {
        set beresp.http.X-VR-ESI = "YES";
      }

      set beresp.http.X-Debug-Forwarded-For = bereq.http.X-Forwarded-For;

      if ( bereq.http.X-UA-Device )
      {
        set beresp.http.X-UA-Device = bereq.http.X-UA-Device;
      }

      # If we have received a custom header indicating the protocol in the request we can show it:
      if ( bereq.http.X-Forwarded-Proto )
      {
        set beresp.http.X-Forwarded-Proto = bereq.http.X-Forwarded-Proto;
      }

      if ( bereq.http.X-VR-Rule-Wordpress )
      {
        set beresp.http.X-VR-Rule-Wordpress = bereq.http.X-VR-Rule-Wordpress;
      }

      set beresp.http.X-VR-Debug-TTL = beresp.ttl;
      set beresp.http.X-VR-Debug-Grace = beresp.grace;
      set beresp.http.X-VR-Debug-Keep = beresp.keep;
      set beresp.http.X-Cache-req-uncacheable = bereq.uncacheable;
      set beresp.http.X-Cache-resp-uncacheable = beresp.uncacheable;

      // if ( bereq.http.X-VR-Debug-Msg )
      // {
      //   set beresp.http.X-VR-Debug-Msg = bereq.http.X-VR-Debug-Msg + beresp.http.X-VR-Debug-Msg;
      // }
    }
  }
}

sub vcl_deliver {
  # Happens when we have all the pieces we need, and are about to send the
  # response to the client.

  if ( req.http.X-VR-Enabled )
  {
    # Add debug header to see if it's a HIT/MISS and the number of hits, disable when not needed
    if ( obj.hits > 0 )
    {
      set resp.http.X-VR-Cache = "HIT";
    }
    else
    {
      set resp.http.X-VR-Cache = "MISS";
    }

    if ( req.http.X-VR-Debug )
    {
      # Please note that obj.hits behavior changed in 4.0, now it counts per object head, not per object
      # and obj.hits may not be reset in some cases where bans are in use. See bug 1492 for details.
      # So take hits with a grain of salt
      set resp.http.X-Cache-Hits = obj.hits;
      set resp.http.X-VR-Grace = req.http.X-VR-Grace;

      if ( req.http.X-VR-Force-Grace )
      {
        set resp.http.X-VR-Force-Grace = req.http.X-VR-Force-Grace;
      }
    
      if ( req.http.X-VR-Force-Miss )
      {
        set resp.http.X-VR-Force-Miss = req.http.X-VR-Force-Miss;
      }  

      set resp.http.X-VR-Restarts = req.restarts;
      
      // if ( req.http.X-VR-Debug-Msg || resp.http.X-VR-Debug-Msg )
      // {
      //   if ( req.http.X-VR-Debug-Msg )
      //   {
      //     set resp.http.X-VR-Debug-Msg = resp.http.X-VR-Debug-Msg + ", " + req.http.X-VR-Debug-Msg;
      //   }
        
      //   set resp.http.X-VR-Debug-Msg = regsub(req.http.X-VR-Debug-Msg, "^[\, ]+", "");
      // }
      if ( req.http.X-VR-Debug-Msg )
      {
        set resp.http.X-VR-Debug-Msg = resp.http.X-VR-Debug-Msg + req.http.X-VR-Debug-Msg;
        # set resp.http.X-VR-Debug-Msg-Req = regsub(req.http.X-VR-Debug-Msg, "^[\, ]+", "");
      }
      
      if ( resp.http.X-VR-Debug-Msg )
      {
        set resp.http.X-VR-Debug-Msg = regsub(resp.http.X-VR-Debug-Msg, "^[\, ]+", "");
      }
    } 
    else
    {
      unset resp.http.X-Varnish;
      unset resp.http.Via;

      unset resp.http.X-VR-Debug-Msg;

      unset resp.http.X-VR-Cache;
      unset resp.http.X-VR-Default-TTL;
      unset resp.http.X-VR-Default-Grace;
    }
  }
}
