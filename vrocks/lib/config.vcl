import var;

sub vcl_backend_response {
  if ( ! bereq.http.X-VR-Enabled )
  {
    # no need to go further
    set beresp.http.X-VR-Debug-Msg = "VR Disabled " + bereq.http.X-VR-Debug-Msg + beresp.http.X-VR-Debug-Msg;
    return (pass);
  }
}

sub vcl_recv {
  if ( req.restarts == 0 )
  {
    # Protect from client setting this
    # only internally set and used
    unset req.http.X-VR-Grace;
    unset req.http.X-VR-Obj-Hit;
    unset req.http.X-VR-Force-Miss;
    unset req.http.X-VR-Force-Grace;
    unset req.http.X-VR-Req-Cacheable;
  }

  if ( client.ip ~ upstream_proxy )
  {
    # by default, varnish is enabled
    if ( req.http.X-VR-Enabled == "false" )
    {
      unset req.http.X-VR-Enabled;
    }
    else
    {
      set req.http.X-VR-Enabled = "true";
    }
  }
  elseif ( req.restarts == 0 )
  {
    # req is not in our upstream_proxy

    unset req.http.X-VR-Enabled;
    unset req.http.X-VR-Debug;
    unset req.http.X-VR-Retry-If-Error;
    unset req.http.X-VR-Stale-If-Bot;
    unset req.http.X-VR-Stale-If-Error;
    unset req.http.X-VR-Rule-Wordpress;
    unset req.http.X-VR-Rule-Prestashop;
    unset req.http.X-VR-Clear-Cookie-All;
    unset req.http.X-VR-Clear-Cookie-PHPSESSID;
    unset req.http.X-VR-Cache-Ajax;
    unset req.http.X-VR-Cache-Static;
    unset req.http.X-VR-Force-Reload-Allow;
    unset req.http.X-VR-Default-TTL;
    unset req.http.X-VR-Default-Grace;
    unset req.http.X-VR-Default-Keep;
    unset req.http.X-VR-Clean-Cache-Control;
    unset req.http.X-VR-Query-String-Sort;

    return (synth(403,"Forbidden"));
  }

  if ( req.http.X-VR-Enabled )
  {
    if ( req.http.X-VR-Debug != "true" )
    {
      unset req.http.X-VR-Debug;
    }

    # Stale-If-Error default true
    if ( req.http.X-VR-Stale-If-Error == "false" )
    {
      unset req.http.X-VR-Stale-If-Error;
      # requires stale-if-error
      unset req.http.X-VR-Stale-If-Bot;
    }
    elseif ( ! req.http.X-VR-Stale-If-Error ) {
      set req.http.X-VR-Stale-If-Error = "true";
    }
    
    # Stale-If-Bot default false 
    if ( req.http.X-VR-Stale-If-Bot )
    {
      if ( req.http.X-VR-Stale-If-Bot == "true" )
      {
        set req.http.X-VR-Stale-If-Bot = 1d;
      }
      elseif ( std.duration(req.http.X-VR-Stale-If-Bot, 0s) <= 0s )
      {
        unset req.http.X-VR-Stale-If-Bot;
      }
      elseif ( std.duration(req.http.X-VR-Stale-If-Bot, 0s) > 10d )
      {
        set req.http.X-VR-Stale-If-Bot = 10d;
      }
    }

    # default false
    if (
      req.http.X-VR-Query-String-Sort &&
      req.http.X-VR-Query-String-Sort != "true"
    )
    {
      unset req.http.X-VR-Query-String-Sort;
    }

    # default false
    if (
      req.http.X-VR-Retry-If-Error &&
      req.http.X-VR-Retry-If-Error != "true"
    )
    {
      unset req.http.X-VR-Retry-If-Error;
    }

    # default false
    if (
      req.http.X-VR-Rule-Wordpress &&
      req.http.X-VR-Rule-Wordpress != "true"
    )
    {
      unset req.http.X-VR-Rule-Wordpress;
    }

    # default false - obsolete
    # if ( req.http.X-VR-Rule-Prestashop != "true" )
    # {
    #   unset req.http.X-VR-Rule-Prestashop;
    # }

    # default false
    if (
      req.http.X-VR-Cache-Ajax  &&
      req.http.X-VR-Cache-Ajax != "true" )
    {
      unset req.http.X-VR-Cache-Ajax;
    }

    # default false
    if (
      req.http.X-VR-Cache-Static &&
      req.http.X-VR-Cache-Static != "true" )
    {
      unset req.http.X-VR-Cache-Static;
    }

    # default false
    if ( 
      req.http.X-VR-Clear-Cookie-All  &&
      req.http.X-VR-Clear-Cookie-All != "true"
    )
    {
      unset req.http.X-VR-Clear-Cookie-All;
    }
    else
    {
      # we clear all cookies, so no need for this
      unset req.http.X-VR-Clear-Cookie-PHPSESSID;
    }

    if (
      req.http.X-VR-Clear-Cookie-PHPSESSID &&
      req.http.X-VR-Clear-Cookie-PHPSESSID != "true"
    )
    {
      unset req.http.X-VR-Clear-Cookie-PHPSESSID;
    }
    
    # by default don't clean Cache-Control
    if (
      req.http.X-VR-Clean-Cache-Control &&
      req.http.X-VR-Clean-Cache-Control != "true"
    )
    {
      unset req.http.X-VR-Clean-Cache-Control;
    }

    # X-VR-Force-Reload-Allow active by default
    # it could be considered a safety breach as
    # it makes it easy to overload backend
    # but note that even with backend overloaded, site should stay accessible
    # and same could be achieved by calling /wp-admin ... is wp a security breach ?
    if ( req.http.X-VR-Force-Reload-Allow == "false" )
    {
      unset req.http.X-VR-Force-Reload-Allow;
    }
    elseif( ! req.http.X-VR-Force-Reload-Allow )
    {
      set req.http.X-VR-Force-Reload-Allow = "true";
    }
  }
}
