import std;


# Watch out on confusion between req.http.url and obj.http.url

# Regex purging
# Treat the request URL as a regular expression.
# sub purge_regex {
#   ban( "req.http.host == " + req.http.host + " && req.url ~ ^" + req.url );
# }

# sub purge_regex {
#   ban( "req.http.host == " + req.http.host + " && req.url ~ ^" +
#     regsub(
#       regsuball( # dbl quotes
#         regsuball( # /* => /.*
#           regsuball( # . => \.
#             req.url,
#             "\.",
#             "\\."
#             # "(?i)\.[a-z]+$", # escape dots when .xyz$
#             # "\\" + "\0" + "(\\?|$)"  # to \.xyz(\?|$)
#           ),
#           "\*",
#           ".*"
#         ),
#         "\x22",  # escape double quotes.  (normally they get a forbidden in normalize.vcl)
#         "\\" + "\x22" # or if it's required
#       ),
#       "^\^", ""
#     )
#   );
# }

# Exact purging
# Use the exact request URL (including any query params)
# sub purge_exact {
#   ban( "req.http.host == " + req.http.host
#     + " && req.url == " + req.url );
# }

# Page purging (default)
# Use the exact request URL, but ignore any query params
# sub purge_page {
#   ban( "req.http.host == " + req.http.host
#     + " && req.url ~ ^" + regsub(req.url, "\?.*$", "") + "(\?|$)" );
# }

sub vcl_recv {
  # dummy to avoid error acl not used
  # if ( false
  #   && (
  #     client.ip ~ upstream_proxy
  #     || client.ip ~ purge_acl
  #   ) ) {}

  if (
    req.method == "PURGE" ||
    req.method == "BAN"
  )
  {
    if (
      std.ip(req.http.X-Real-IP, "0.0.0.0") ~ purge_acl ||
      (
        # safe if port is only accessible from localhost
        std.port(server.ip) == 6091
      )
    )
    {
      # Do our best to figure out which one they want.
      # should we allow brackets? () - see normalize.vcl
      if ( req.url ~ "\^" )
      {
        if (
          std.ban(
            "req.http.host == " + req.http.host
            + " && req.url ~ " + req.url
          )
        )
        {
          return (
            synth(
              200,
              "Purged regex: \n" +
              "req.http.host == " + req.http.host
              + " && req.url ~ " + req.url
            )
          );
        }
        else
        {
          # return ban error in 400 response
          return (synth(400, std.ban_error()));
        }
      }
      elseif ( req.url ~ "[\\*+$|()]" ) 
      {
        # call purge_regex;
        if (
          std.ban(
            "req.http.host == " + req.http.host
            + " && req.url ~ ^" + regsub(req.url, "^\^" , "")
          )
        )
        {
          return (
            synth(
              200,
              "Purged regex: \n" +
              "req.http.host == " + req.http.host
              + " && req.url ~ ^" + regsub(req.url, "^\^" , "")

              # + " " + "'" + "^" + 
              # regsub(
              #   regsuball( # dbl quotes
              #     regsuball( # /* => /.*
              #       regsuball( # . => \.
              #         req.url,
              #         "\.",
              #         "\\."
              #         # "(?i)\.[a-z]+$", # escape dots when .xyz$
              #         # "\\" + "\0" + "(\\?|$)"  # to \.xyz(\?|$)
              #       ),
              #       "\*",
              #       ".*"
              #     ),
              #     "\x22",  # escape double quotes.  (normally they get a forbidden in normalize.vcl)
              #     "\\" + "\x22" # or if it's required
              #   ),
              #   "^\^", ""
              # ) +
              # "'" # html entities "&#34;" - "\x22" and "%22" didn't work
            )
          );
        }
        else
        {
          # return ban error in 400 response
          return (synth(400, std.ban_error()));
        }
      }
      elsif ( req.url ~ "\?" )
      {
        # call purge_exact;
        if(
          std.ban(
            "req.http.host == " + req.http.host
            + " && req.url == " + req.url
          )
        )
        {
          return (
            synth(
              200,
              "Purged exact: \n" +
              "req.http.host == " + req.http.host
              + " && req.url == " + req.url
            )
          );
        }
        else
        {
          # return ban error in 400 response
          return (synth(400, std.ban_error()));
        }
      }
      else
      {
        # call purge_page;
        if (
          std.ban(
            "req.http.host == " + req.http.host
            + " && req.url ~ ^" +
              regsub(
                # Ignore trailing slash
                # remove trailing slash and add optional slash
                # /exemple => /exemple/?
                req.url, "^(/.*?)/?$", "\1/?"
              ) +
              "(\?|$)"
          )
        )
        {
          return (
            synth(
              200,
              "Purged page: \n" +
              "req.http.host == " + req.http.host
              + " && req.url ~ ^" +
                regsub(
                  # Ignore trailing slash
                  # remove trailing slash and add optional slash
                  # /exemple => /exemple/?
                  req.url, "^(/.*?)/?$", "\1/?"
                ) +
                "(\?|$)"
            )
          );
        }
        else
        {
          # return ban error in 400 response
          return (synth(400, std.ban_error()));
        }
      }
    }
    else
    {
      return (synth(403, "Not allowed"));
    }
  }
}
