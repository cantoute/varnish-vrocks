
import header;

include "vrocks/lib/dont-cache.vcl";
include "vrocks/lib/wordpress.vcl";
# include "vrocks/lib/prestashop.vcl";

sub vcl_recv {
  # it seems we are on the way to cache
  # remove not needed data from req (cookies, query string)
  call req_cleanup;

  # we still have cookies ?
  # then don't cache
  if ( req.http.Cookie )
  {
    if ( req.http.X-VR-Debug )
    {
      set req.http.X-VR-Debug-Msg = req.http.X-VR-Debug-Msg + ", pass: has cookie in request";
    }
    return (pass);
  }

  # return (hash);
}


sub vcl_hash {

  # to force cache refresh
  # hash_data("z");

  if ( req.http.host )
  {
    hash_data(req.http.host);
  }
  else
  {
    hash_data(server.ip);
  }

  hash_data( req.url );

  # If the client supports compression, keep that in a different cache
  # behind nginx, this should always be set to gzip
  if ( req.http.Accept-Encoding )
  {
    hash_data(req.http.Accept-Encoding);
  }

  # Include the X-Forward-Proto header, since we want to treat HTTPS
  # requests differently, and make sure redirect headers are passed properly
  if ( req.http.X-Forwarded-Proto )
  {
    hash_data(req.http.X-Forwarded-Proto);
  }

  # TODO: hash based on Vary user-agent 
  # could it be smart and actually base on device-detect ?
#   if ( req.http.Cookie ) {
#     # Include cookie in cache hash
#     hash_data(req.http.Cookie);
#   }

  # Example for caching multiple object versions by device previously
  # detected (when static content could also vary):
  // if ( req.http.X-UA-Device )
  // {
  //   hash_data(req.http.X-UA-Device);
  // }

  ## builtin vcl_hash (varnish 6.1)
  # hash_data(req.url);
  # if (req.http.host) {
  #   hash_data(req.http.host);
  # } else {
  #   hash_data(server.ip);
  # }

  # no need to run builtin vcl_hash so
  // return (lookup);
}

// sub vcl_hit {
  # if ( obj.ttl > 0s ) {
  #   # normal hit
  #   # set req.http.X-VR-Grace = "NONE";
  #   return (deliver);
  # } elseif ( obj.ttl + obj.grace > 0s ) {
  #   # Object is in grace, deliver it
  #   # Automatically triggers a background fetch
  #   # set req.http.X-VR-Grace = "GRACE";
  #   return (deliver);
  # } elseif ( !std.healthy(req.backend_hint) ) {
  #   # set req.http.X-VR-Grace = "FULL (backend sick)";
  #   return (deliver);
  # }
  # sounds like there isn't a clean way to do this in varnish 6.x yet
  # elseif ( req.http.User-Agent ~ "(?i)(bot|spider|search|MSIE|tracker)" ) {
  #   # We've got an object in cache but that is in "keep" mode

  #   # if it's a bot, anything would be good enough
  #   set req.http.X-VR-Grace = "FULL (bots)";

  #   # with varnish 6.x seems like return (deliver) here will result in a fetch
  #   # as backend is healthy
  #   # return (deliver);

  #   # we could hook onto same path as lib/backend-failed-retry-cache.vcl
  #   # but then cache isn't updated in background

  #   # set req.http.X-VR-Stale-If-Error = "true";
  #   # return (restart);
  # }

  # return (miss);
  # depreciated in 6.0
  # so we restart and set in vcl_recv for the restart (when req.restarts has increased),
  # see - set req.hash_always_miss = true;
  # return (restart);

  // if ( std.healthy(req.backend_hint) ) {
  //   if ( obj.grace < 10d ) {
  //     return (fetch);
  //   }
  // }
  // else
  // {
  //   if ( req.http.X-VR-Debug ) {
  //     set req.http.X-VR-Debug-Msg = req.http.X-VR-Debug-Msg + ", backend_hint sick";
  //   }
  // }
// }

sub vcl_backend_response {
  if ( bereq.uncacheable )
  {
    set beresp.ttl = 0s;
  }
  elseif ( beresp.uncacheable )
  {
    # Hit for pass
    # TODO: verify if needed
    # suspecting it overrides cache headers
    # when cache updates during grace period
    // set beresp.ttl = 2m;
    // set beresp.grace = 10s;
  }
  elseif ( beresp.ttl > 0s )
  {
    # cacheable so we remove Set-Cookie
    unset beresp.http.Set-Cookie;

    # if missing add Last-Modified
    if ( !beresp.http.Last-Modified )
    {
      set beresp.http.Last-Modified = "" + now;
    }

    if (
      beresp.ttl == 120s &&
      beresp.status == 301 &&
      bereq.http.X-VR-Stale-If-Error
    )
    {
      # overrides varnish defaults for 301 response
      set beresp.ttl = 1d;

      if ( bereq.http.X-VR-Debug )
      {
        set beresp.http.X-VR-Debug-Msg = beresp.http.X-VR-Debug-Msg + ", setting default ttl to " + beresp.ttl;
      }
    }

    # by default ttl = 2m
    if (
      beresp.ttl == 120s &&
      std.duration(bereq.http.X-VR-Default-TTL, 120s) != beresp.ttl 
    )
    {
      # X-VR-Default-TTL header overrides varnish defaults
      set beresp.ttl = std.duration(bereq.http.X-VR-Default-TTL, 120s);

      if ( bereq.http.X-VR-Debug )
      {
        set beresp.http.X-VR-Debug-Msg = beresp.http.X-VR-Debug-Msg + ", setting default ttl to " + beresp.ttl;
      }
    }

    # by default garce = 10s
    if (
      beresp.grace == 10s &&
      std.duration(bereq.http.X-VR-Default-Grace, 10s) != beresp.grace
    )
    {
      # X-VR-Default-Grace header overrides varnish defaults
      # if unset, we change default grace to 2m
      set beresp.grace = std.duration(bereq.http.X-VR-Default-Grace, 10s);

      if ( bereq.http.X-VR-Debug )
      {
        set beresp.http.X-VR-Debug-Msg = beresp.http.X-VR-Debug-Msg + ", setting default grace to " + beresp.grace;
      }
    }

    # moved to stale-if-error.terminate
    # # only for 304 ?
    # # if ( beresp.keep <= 0s )
    # # {
    #   # sets default keep to 100 days
    #   if ( beresp.status < 500 )
    #   {
    #     set beresp.keep = std.duration(bereq.http.X-VR-Default-Keep, 10d);
    #   }

    #   if ( bereq.http.X-VR-Debug )
    #   {
    #     set beresp.http.X-VR-Debug-Msg = beresp.http.X-VR-Debug-Msg + ", setting default keep to " + beresp.keep;
    #   }
    # # }
  }
  else
  {
    # cacheable with ttl == 0 ?
    # dead code - safeguard
    # set beresp.grace = 10s;
  }
}

sub vcl_synth {
  if ( resp.status == 720 )
  {
    # We use this special error status 720 to force redirects with 301 (permanent) redirects
    # To use this, call the following from anywhere in vcl_recv: return (synth(720, "http://host/new.html"));
    set resp.http.Location = resp.reason;
    set resp.status = 301;

    return (deliver);
  }
  
  if ( resp.status == 721 )
  {
    # And we use error status 721 to force redirects with a 302 (temporary) redirect
    # To use this, call the following from anywhere in vcl_recv: return (synth(720, "http://host/new.html"));
    set resp.http.Location = resp.reason;
    set resp.status = 302;

    return (deliver);
  }
}

