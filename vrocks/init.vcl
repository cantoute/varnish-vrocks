
import std;

include "vrocks/lib/purge.vcl";

include "vrocks/lib/normalize.vcl";
include "vrocks/lib/x-forwarded-for.vcl";

include "vrocks/lib/config.vcl";

include "vrocks/lib/devicedetect.vcl";
include "vrocks/lib/req-cleanup.vcl";
sub vcl_recv {
  # dummy call to avoid "not used" error
  if ( false ) {
    call devicedetect;
    call req_cleanup; 
  }
}

include "vrocks/lib/debug.init.vcl";

include "vrocks/lib/bigfiles.vcl";
include "vrocks/lib/static.vcl";

include "vrocks/lib/backend-error.vcl";

include "vrocks/lib/stale-if-error.init.vcl";

include "vrocks/lib/force-reload.vcl";

sub vcl_recv {
  if ( req.http.X-VR-Force-Grace )
  {
    unset req.http.X-VR-Force-Miss;
    unset req.http.Cache-Control;
    unset req.http.Pragma;

    set req.hash_always_miss = false;
  }
  elseif (
    req.http.X-VR-Force-Miss
  ) {
    set req.hash_always_miss = true;
  }
}

// TODO: for now all the code is mixed up with stale-if-error
// include "vrocks/lib/retry-if-error.vcl";


include "vrocks/lib/esi.vcl";


